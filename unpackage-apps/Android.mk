LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := unpackage-apps
LOCAL_MODULE_TAGS := optional

LOCAL_OVERRIDES_PACKAGES := CellBroadcastServiceModulePlatform \
        CellBroadcastAppPlatform \


include $(BUILD_PHONY_PACKAGE)

PACKAGES.$(LOCAL_MODULE).OVERRIDES := $(strip $(LOCAL_OVERRIDES_PACKAGES))

# Include all the makefiles for subdirectories.
include $(call all-makefiles-under,$(LOCAL_PATH))