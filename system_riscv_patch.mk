# FIXME  libcompiler_rt cannot be installed automotically, because there is no
# renderscript in riscv android platform
PRODUCT_PACKAGES += libcompiler_rt
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/lib64/libcompiler_rt.so \
    system/app/PlatformCaptivePortalLogin/% \
    system/priv-app/InProcessNetworkStack/% \
    system/priv-app/PlatformNetworkPermissionConfig/% \
    system/apex/com.android.tethering.inprocess.capex \
    system/priv-app/CellBroadcastServiceModulePlatform/% \

PRODUCT_SYSTEM_PROPERTIES += \
    config.disable_renderscript=1 \
    ro.telephony.default_network=9

ifeq ($(call math_gt,$(PLATFORM_VERSION_LAST_STABLE),12),true)
# Workaround for AOSP mainline
ALLOW_MISSING_DEPENDENCIES := true
TARGET_SUPPORTS_OMX_SERVICE := false
PRODUCT_USES_DEFAULT_ART_CONFIG := false

PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.usejit=false

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    root/init.zygote64.rc
endif

